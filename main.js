// Make body always full height
function bodyFullHeight() {
    document.querySelector('body').style.height = window.innerHeight + "px";
}

window.addEventListener('load', bodyFullHeight);
window.addEventListener('resize', bodyFullHeight);


//Game Rules

/***************************************************************************************************** 

    Players take turns to roll the dice, when rolled the total of the dice gets added to
    the current total for that player.

    If the player rolls a 1 then their current total gets returned to 0 and it is then player 2's turn.

    Players can choose to hold their current total and pass their turn to the other player.

    First to 50 wins.

******************************************************************************************************/

//Game Code
var currentPlayer, currentScore, scores, playing;
currentPlayer = 0;
currentScore = 0;
scores = [0, 0];
winningScore = 50;
playing = true;

function newGame(){
    playing = true;
    scores = [0, 0];
    currentScore = 0;
    document.querySelector('#name-0').textContent = 'Player 1';
    document.querySelector('#name-1').textContent = 'Player 2';
    var nums = document.querySelectorAll('.player-score, .player-current-score');
    nums.forEach(function(num){
        num.textContent = '0';
    });
}

function winner(){
    document.querySelector('#name-' + currentPlayer).textContent = 'WINNER!';
    document.querySelector('#score-' + currentPlayer).textContent = currentScore;
    playing = false;
}

function changePlayer() {
    // Remove active class from current player
    document.querySelector('.player-' + currentPlayer + '-panel').classList.remove('active');

    // Change current player.
    currentPlayer === 0 ? currentPlayer = 1 : currentPlayer = 0;

    // Add active class to new current player
    document.querySelector('.player-' + currentPlayer + '-panel').classList.add('active');
}

document.querySelector('#new-game').addEventListener('click', newGame);

// Roll button functionality.
document.querySelector('#roll-dice').addEventListener('click', function(){
    if(playing) {

        var diceRoll = diceRoll = Math.floor(Math.random() * 6) + 1;
        document.querySelector('#dice').src = 'dice/dice-' + diceRoll + '.svg';

        if(diceRoll !== 1) {
            currentScore += diceRoll;
            document.querySelector('#current-' + currentPlayer).textContent = currentScore;
            if(currentScore >= winningScore || (currentScore + scores[currentPlayer]) >= winningScore) {
                winner();
            }
        } else {

            document.querySelector('#current-' + currentPlayer).textContent = 0;
            document.querySelector('#score-' + currentPlayer).textContent = 0;
            scores[currentPlayer] = 0;

            changePlayer();

            currentScore = 0;
        }
    }
});

//Hold button functionality.
document.querySelector('#hold').addEventListener('click', function(){

    if(playing) {

        scores[currentPlayer] += currentScore;
        document.querySelector('#score-' + currentPlayer).textContent = scores[currentPlayer];

        changePlayer();

        currentScore = 0;
    }

});




